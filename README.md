# Laravel with Lando and a template

This git could be used to create a website starting from a template 

### Steps to reproduce
 1. Create new project: 
    ``` sh
    laravel new project-name && cd project-name && php artisan serve
    ```
 2. Init git:
    ``` sh 
    git init
    ```


### Sources

templates:
* [html5up.net](www.html5up.net)